﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Fido_Main.Fido_Support.ErrorHandling;
using Fido_Main.Fido_Support.Objects.Fido;
using Fido_Main.Main.Receivers;

namespace Fido_Main
{
    public static class Fido
    {
        public static void Execute(Func<bool> execute)
        {
            bool firstLoop = false;

            while (execute())
            {
                Console.Clear();
                var sAppStartupPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\data\fido.db";
                if (!File.Exists(sAppStartupPath))
                {
                    Console.WriteLine(@"Failed to load FIDO DB.");
                    return;
                }
                else
                {
                    if (!firstLoop) Console.WriteLine(@"Loaded FIDO DB successfully.");
                }

                //Load fido configs from database
                Object_Fido_Configs.LoadConfigFromDb("config");

                //Setup syslog
                var server1 = Object_Fido_Configs.GetAsString("fido.logger.syslog.server", "localhost");
                var port1 = Object_Fido_Configs.GetAsInt("fido.logger.syslog.port", 514);
                var facility1 = Object_Fido_Configs.GetAsString("fido.logger.syslog.facility", "local1");
                var sender1 = Object_Fido_Configs.GetAsString("fido.logger.syslog.sender", "Fido");
                var layout1 = Object_Fido_Configs.GetAsString("fido.logger.syslog.layout", "$(message)");
                //SysLogger.Setup(server1, port1, facility1, sender1, layout1);

                //Beginning of primary area which starts parsing of alerts.
                var isParamTest = Object_Fido_Configs.GetAsBool("fido.application.teststartup", true);
                var sDetectors = Object_Fido_Configs.GetAsString("fido.application.detectors", string.Empty).Split(',');

                try
                {
                    if (!firstLoop) Console.WriteLine(isParamTest ? @"Running test configs." : @"Running production configs.");

                    foreach (var detect in sDetectors)
                    {
                        var parseConfigs = Object_Fido_Configs.ParseDetectorConfigs(detect);
                        //Get the detector, ie, email, log, web service, etc.
                        var sDetectorType = parseConfigs.DetectorType;
                        switch (sDetectorType)
                        {
                            case "api":
                                Console.WriteLine(@"Loading webservice receiver.");
                                Recieve_API.DirectToEngine(sDetectorType, detect);
                                break;

                            case "log":
                                Console.WriteLine(@"Loaded log receiver.");
                                var sDefaultServer = parseConfigs.Server;
                                var sDefaultFile = parseConfigs.File;
                                var sVendor = parseConfigs.Vendor;
                                Receive_Logging.DirectToEngine(detect, sVendor, sDefaultServer, sDefaultFile,
                                    isParamTest);
                                break;

                            case "sql":
                                Console.WriteLine(@"Loaded sql receiver.");
                                Receive_SQL.DirectToEngine(sDetectorType, detect);
                                break;

                            case "email":
                                Console.WriteLine(@"Loaded email receiver.");
                                var sEmailVendor = Object_Fido_Configs.GetAsString("fido.email.vendor", "imap");
                                var sDetectorsEmail = parseConfigs.EmailFrom;
                                var sDetectorsFolder = parseConfigs.Folder;
                                Receive_Email.ReadEmail(sEmailVendor, sDetectorsFolder, null, sDetectorsEmail,
                                    isParamTest);
                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                    Fido_EventHandler.SendEmail("Fido Error", "Fido Failed: {0} Exception caught in fidomain area:" + e);
                }

                //Sleep for X # of seconds per iteration specified in Fido configuration 
                var iSleep = Object_Fido_Configs.GetAsInt("fido.application.sleepiteration", 5);
                if (!firstLoop) Console.WriteLine(@"Fido processing complete... sleeping for " + (iSleep/1000).ToString(CultureInfo.InvariantCulture) + @" seconds.");
                Thread.Sleep(1000);

                firstLoop = true;
            }
        }
    }
}
