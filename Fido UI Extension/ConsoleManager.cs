﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace Fido_UI_Extension
{
    [SuppressUnmanagedCodeSecurity]
    public static class ConsoleManager
    {
        private const int SW_HIDE = 0;
        private const int SW_SHOW = 5;

        private static bool HasConsole => GetConsoleWindow() != IntPtr.Zero;

        public static ConsoleTextWriter ConsoleOut { get; } = new ConsoleTextWriter();

        /// <summary>
        ///     Creates a new console instance if the process is not attached to a console already.
        /// </summary>
        public static void Setup()
        {
            if (!HasConsole)
            {
                AllocConsole();
                InvalidateOutAndError();

                Console.SetOut(ConsoleOut);

                ShowWindow(GetConsoleWindow(), SW_HIDE);
            }
        }
        [DllImport("kernel32.dll")]
        private static extern bool AllocConsole();

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        private static void InvalidateOutAndError()
        {
            var type = typeof (Console);

            var _out = type.GetField("_out", BindingFlags.Static | BindingFlags.NonPublic);
            var error = type.GetField("_error", BindingFlags.Static | BindingFlags.NonPublic);
            var initializeStdOutError = type.GetMethod("InitializeStdOutError", BindingFlags.Static | BindingFlags.NonPublic);

            Debug.Assert(_out != null);
            Debug.Assert(error != null);

            Debug.Assert(initializeStdOutError != null);

            _out.SetValue(null, null);
            error.SetValue(null, null);

            initializeStdOutError.Invoke(null, new object[] {true});
        }
    }

    public class ConsoleTextWriter : TextWriter
    {
        private readonly List<string> _lines = new List<string>();

        public override Encoding Encoding => Encoding.Default;

        public event Action<string> OnWriteLine;

        public override void WriteLine(string value)
        {
            _lines.Add(value);

            OnWriteLine?.Invoke(value);
        }

        public string[] GetLines()
        {
            return _lines.ToArray();
        }
    }
}