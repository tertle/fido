﻿using System;
using System.Windows.Controls;

namespace Fido_UI_Extension.Pages
{
    /// <summary>
    /// Interaction logic for ChartsPage.xaml
    /// </summary>
    public partial class MainPage : UserControl
    {
        private readonly ChartViewModel _cvm = new ChartViewModel();

        public MainPage()
        {
            InitializeComponent();

            DataContext = _cvm;

            ChartTest.LegendItems.Clear();

            foreach(var l in _cvm.Legend)
                ChartTest.LegendItems.Add(l);
        }

        public void PeriodChanged(object sender, EventArgs eventArgs)
        {
            var s = (ComboBox) sender;
            _cvm.PeriodChanged(s.Text);
        }
    }
}
