﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Controls;
using Fido_UI_Extension.Controls;
using Fido_UI_Extension.Data;

namespace Fido_UI_Extension.Pages.Settings
{
    /// <summary>
    /// Interaction logic for Appearance.xaml
    /// </summary>
    public partial class FidoUser : UserControl
    {
        public FidoUser()
        {
            InitializeComponent();

            var configValues = DatabaseManager.ReadFromTable(@"SELECT key, value from config")
                    .AsEnumerable()
                    .ToDictionary(row => row.Field<string>(0), row => row.Field<string>(1));

            var configs = new List<ConfigItem>
            {
                ConfigItem.Create("titles", configValues["fido.posture.user.titles"], "Comma separated string value listing the critical titles in the organization. For example, Chief Executive Office, or CEO, because of their status or access. Another example would be Windows Server Engineer because the user would have Domain Admin access. This value is pulled from LDAP and should match what is listed in this resource."),
                ConfigItem.Create("department", configValues["fido.posture.user.department"], "Comma separated string value listing the critical departments in the organization. For example, Finance because of their access to financial information, or Customer Service because they could have access to credit card (PCI) information."),
                ConfigItem.Create("titlescoreweight", configValues["fido.posture.user.titlescoreweight"], "Integer value added to the user posture score if the title value is found.", typeof(int)),
                ConfigItem.Create("departmentscoreweight", configValues["fido.posture.user.departmentscoreweight"], "Integer value added to the user posture score if the department value is found.", typeof(int)),
            };
            
            var sql = "update config set value = '{1}' where key = 'fido.posture.user.{0}';";

            ConfigBuilder.Create(ConfigGrid, configs, sql);
        }

        
    }
}
