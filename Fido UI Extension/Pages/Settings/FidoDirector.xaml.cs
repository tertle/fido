﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Controls;
using Fido_UI_Extension.Controls;
using Fido_UI_Extension.Data;

namespace Fido_UI_Extension.Pages.Settings
{
    /// <summary>
    /// Interaction logic for Appearance.xaml
    /// </summary>
    public partial class FidoDirector : UserControl
    {
        public FidoDirector()
        {
            InitializeComponent();

            var configValues = DatabaseManager.ReadFromTable(@"SELECT key, value from config")
                    .AsEnumerable()
                    .ToDictionary(row => row.Field<string>(0), row => row.Field<string>(1));

            var configs = new List<ConfigItem>
            {
                ConfigItem.Create("hostdetection", configValues["fido.director.hostdetection"], "If set to true, the director will run host detection on machine being targeted, recommended to keep this value as true.", typeof(bool)),
                ConfigItem.Create("runinventory", configValues["fido.director.runinventory"], "If set to true, the director will attempt to get detailed information about the target machine, such as the operating system and antivirus running.", typeof(bool)),
                ConfigItem.Create("userdetect", configValues["fido.director.userdetect"], "If set to true, the director will get information about the user the machine is primarily used by.", typeof(bool)),
                ConfigItem.Create("virustotal", configValues["fido.director.virustotal"], "Tells the director if virus total, a free online virus, malware and URL scanner, should be used as a threat feed.", typeof(bool)),
                ConfigItem.Create("threatgrid", configValues["fido.director.threatgrid"], "Tells the director if ThreatGrid should be used as a threat feed.", typeof(bool)),
                ConfigItem.Create("alienvault", configValues["fido.director.alienvault"], "Tells the director if AlientVault should be used as a threat feed.", typeof(bool))
            };
            
            var sql = "update config set value = '{1}' where key = 'fido.director.{0}';";

            ConfigBuilder.Create(ConfigGrid, configs, sql);
        }

        
    }
}
