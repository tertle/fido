﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Controls;
using Fido_UI_Extension.Controls;
using Fido_UI_Extension.Data;

namespace Fido_UI_Extension.Pages.Settings
{
    /// <summary>
    /// Interaction logic for Appearance.xaml
    /// </summary>
    public partial class FidoVirusTotal : UserControl
    {
        public FidoVirusTotal()
        {
            InitializeComponent();

            var configValues = DatabaseManager.ReadFromTable(@"SELECT key, value from config")
                    .AsEnumerable()
                    .ToDictionary(row => row.Field<string>(0), row => row.Field<string>(1));

            var configs = new List<ConfigItem>
            {
                ConfigItem.Create("trojanscore", configValues["fido.securityfeed.virustotal.trojanscore"], "For hash based lookups, the number returns coming back from a hash report specified as a Trojan.", typeof(int)),
                ConfigItem.Create("trojanweight", configValues["fido.securityfeed.virustotal.trojanweight"], "The value added to the threat score if the total returns for Trojans is exceeded for hash based lookups.", typeof(int)),
                ConfigItem.Create("regularscore", configValues["fido.securityfeed.virustotal.regularscore"], "For hash based lookups, the number returns coming back from a hash report specified as a generic malware.", typeof(int)),
                ConfigItem.Create("regularweight", configValues["fido.securityfeed.virustotal.regularweight"], "The value added to the threat score if the total returns for generic malware is exceeded for hash based lookups.", typeof(int)),
                ConfigItem.Create("urlregularscore", configValues["fido.securityfeed.virustotal.urlregularscore"], "For URL based lookups, the number returns coming back from a URL report specified as being a malware site.", typeof(int)),
                ConfigItem.Create("urlregularweight", configValues["fido.securityfeed.virustotal.urlregularweight"], "The value added to the threat score if the total returns for malware site is exceeded.", typeof(int)),
                ConfigItem.Create("detecteddownloadscore", configValues["fido.securityfeed.virustotal.detecteddownloadscore"], "For the correlated IP report, the number of returns which is equal to or greater than before incrementing the threat score.", typeof(int)),
                ConfigItem.Create("detecteddownloadweight", configValues["fido.securityfeed.virustotal.detecteddownloadweight"], "The value added to the threat score if the total returns for downloads from an IP report is exceeded.", typeof(int)),
                ConfigItem.Create("detecteddownloadmultiplier", configValues["fido.securityfeed.virustotal.detecteddownloadmultiplier"], "Integer value used to offset the total number of possible data points which would come back from threat feeds. This value will take the total number accumulated from the IP detected download threat score and multiply it by this value.", typeof(int)),
                ConfigItem.Create("detectedcommScore", configValues["fido.securityfeed.virustotal.detectedcommScore"], "For the correlated IP report, the number of returns which is equal to or greater than before incrementing the threat score.", typeof(int)),
                ConfigItem.Create("detectedcommweight", configValues["fido.securityfeed.virustotal.detectedcommweight"], "The value added to the threat score if the total returns for detected communications from an IP report is exceeded.", typeof(int)),
                ConfigItem.Create("detectedcommmultiplier", configValues["fido.securityfeed.virustotal.detectedcommmultiplier"], "Integer value used to offset the total number of possible data points which would come back from threat feeds. This value will take the total number accumulated from the IP detected communication threat score and multiply it by this value.", typeof(int)),
                ConfigItem.Create("detectedurlscore", configValues["fido.securityfeed.virustotal.detectedurlscore"], "For the correlated IP report, the number of returns which is equal to or greater than before incrementing the threat score.", typeof(int)),
                ConfigItem.Create("detectedurlweight", configValues["fido.securityfeed.virustotal.detectedurlweight"], "The value added to the threat score if the total returns for detected malicious URLs from an IP report is exceeded.", typeof(int)),
                ConfigItem.Create("detectedurlmultiplier", configValues["fido.securityfeed.virustotal.detectedurlmultiplier"], "Integer value used to offset the total number of possible data points which would come back from threat feeds. This value will take the total number accumulated from the IP detected URL threat score and multiply it by this value.", typeof(int)),
                ConfigItem.Create("feedweight", configValues["fido.securityfeed.virustotal.feedweight"], "When multiple feeds are configured this integer value will be used to weight certain feeds either higher or lower than the other feeds.", typeof(int)),
                ConfigItem.Create("apikey", configValues["fido.securityfeed.virustotal.apikey"], "API key used to access VirusTotal to get cloud sourced information about the alert."),
            };
            
            var sql = "update config set value = '{1}' where key = 'fido.securityfeed.virustotal.{0}';";

            ConfigBuilder.Create(ConfigGrid, configs, sql);
        }

        
    }
}
