﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Controls;
using Fido_UI_Extension.Controls;
using Fido_UI_Extension.Data;

namespace Fido_UI_Extension.Pages.Settings
{
    /// <summary>
    /// Interaction logic for Appearance.xaml
    /// </summary>
    public partial class FidoAlienVault : UserControl
    {
        public FidoAlienVault()
        {
            InitializeComponent();

            var configValues = DatabaseManager.ReadFromTable(@"SELECT key, value from config")
                    .AsEnumerable()
                    .ToDictionary(row => row.Field<string>(0), row => row.Field<string>(1));

            var configs = new List<ConfigItem>
            {
                ConfigItem.Create("malwarevalues", configValues["fido.securityfeed.alienvault.malwarevalues"], "Comma separated string values of types that AlienVault should perform analysis on."),
                ConfigItem.Create("riskscorehigh", configValues["fido.securityfeed.alienvault.riskscorehigh"], "The cut-off for which the risk score of the alert should ensure the threat is considered high, between 0 and 100.", typeof(short)),
                ConfigItem.Create("riskscoremedium", configValues["fido.securityfeed.alienvault.riskscoremedium"], "The cut-off for which the risk score of the alert should ensure the threat is considered medium, between 0 and 100 and should be less than the riskscorehigh.", typeof(short)),
                ConfigItem.Create("riskscorelow", configValues["fido.securityfeed.alienvault.riskscorelow"], "The cut-off for which the risk score of the alert should ensure the threat is considered low, between 0 and 100 and should be less than the riskscoremedium.", typeof(short)),
                ConfigItem.Create("riskweighthigh", configValues["fido.securityfeed.alienvault.riskweighthigh"], "The value added to the risk score if the score returned for detected threat is higher than the riskscorehigh.", typeof(int)),
                ConfigItem.Create("riskweightmedium", configValues["fido.securityfeed.alienvault.riskweightmedium"], "The value added to the risk score if the score returned for detected threat is higher than the riskscoremedium.", typeof(int)),
                ConfigItem.Create("riskweightlow", configValues["fido.securityfeed.alienvault.riskweightlow"], "The value added to the risk score if the score returned for detected threat is higher than the riskscorelow.", typeof(int)),
                ConfigItem.Create("reliabilityscorehigh", configValues["fido.securityfeed.alienvault.reliabilityscorehigh"], "The cut-off for which the reliability score of the alert should ensure the threat is considered high, between 0 and 100.", typeof(short)),
                ConfigItem.Create("reliabilityscoremedium", configValues["fido.securityfeed.alienvault.reliabilityscoremedium"], "The cut-off for which the reliability score of the alert should ensure the threat is considered high, between 0 and 100 and should be less than the reliabilityscorehigh.", typeof(short)),
                ConfigItem.Create("reliabilityscorelow", configValues["fido.securityfeed.alienvault.reliabilityscorelow"], "The cut-off for which the reliability score of the alert should ensure the threat is considered high, between 0 and 100 and should be less than the reliabilityscoremedium.", typeof(short)),
                ConfigItem.Create("reliabilityweighthigh", configValues["fido.securityfeed.alienvault.reliabilityweighthigh"], "The value added to the reliability score if the score returned for detected threat is higher than the reliabilityscorehigh.", typeof(int)),
                ConfigItem.Create("reliabilityweightmedium", configValues["fido.securityfeed.alienvault.reliabilityweightmedium"], "The value added to the reliability score if the score returned for detected threat is higher than the reliabilityscoremedium.", typeof(int)),
                ConfigItem.Create("reliabilityweightlow", configValues["fido.securityfeed.alienvault.reliabilityweightlow"], "The value added to the reliability score if the score returned for detected threat is higher than the reliabilityscorelow.", typeof(int))
            };
            
            
            var sql = "update config set value = '{1}' where key = 'fido.securityfeed.alienvault.{0}';";

            ConfigBuilder.Create(ConfigGrid, configs, sql);
        }

        
    }
}
