﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Fido_UI_Extension.Controls;
using Fido_UI_Extension.Data;

namespace Fido_UI_Extension.Pages.Settings
{
    /// <summary>
    /// Interaction logic for Appearance.xaml
    /// </summary>
    public partial class FidoLDAP : UserControl
    {
        public FidoLDAP()
        {
            InitializeComponent();

            var configValues = DatabaseManager.ReadFromTable(@"SELECT key, value from config")
                    .AsEnumerable()
                    .ToDictionary(row => row.Field<string>(0), row => row.Field<string>(1));

            var configs = new List<ConfigItem>
            {
                ConfigItem.Create("basedn", configValues["fido.ldap.basedn"], "Root domain path where information about users is stored."),
                ConfigItem.Create("userid", configValues["fido.ldap.userid"], "Optional username if required to access the domain."),
                ConfigItem.Create("pwd", configValues["fido.ldap.pwd"], "Optional password if required to access the domain."),
            };
            
            var sql = "update config set value = '{1}' where key = 'fido.ldap.{0}';";

            ConfigBuilder.Create(ConfigGrid, configs, sql);
        }

        
    }
}
