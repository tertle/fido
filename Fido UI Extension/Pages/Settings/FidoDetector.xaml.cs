﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.VisualStyles;
using System.Windows.Media;
using Fido_UI_Extension.Controls;
using Fido_UI_Extension.Data;

namespace Fido_UI_Extension.Pages.Settings
{
    /// <summary>
    /// Interaction logic for Appearance.xaml
    /// </summary>
    public partial class FidoDetector : UserControl
    {
        //private Dictionary<long, string> _sDetectors;
        private bool _removeCheck;

        public FidoDetector()
        {
            InitializeComponent();

            Detectors.DropDownClosed += DetectorsOnDropDownClosed;

            CreateMenu(0);
            //Create(_sDetectors.First().Key.ToString());
        }

        private void DetectorsOnDropDownClosed(object sender, EventArgs eventArgs)
        {
            Create();
        }

        private void CreateMenu(int index)
        {
            ChangeRemoveButton(false);

            var sDetectors = DatabaseManager.ReadFromTable(@"SELECT primkey, detector from configs_detectors")
                    .AsEnumerable()
                    .ToDictionary(row => row.Field<long>(0), row => row.Field<string>(1));

            Detectors.Items.Clear();

            foreach (var d in sDetectors)
            {
                Detectors.Items.Add(new TextBlock() { Text = $"({d.Key}) {d.Value}" });
            }

            Detectors.SelectedIndex = index;

            Create();
        }

        private void Create()
        {
            if (string.IsNullOrEmpty(Detectors.Text))
            {
                ConfigGrid.Children.Clear();
                return;
            }

            var key = Detectors.Text.Split('(', ')')[1];

            var query = @"SELECT * from configs_detectors WHERE primkey = '" + key + @"'";
            DataRow a = DatabaseManager.ReadFromTable(query).AsEnumerable().First();

            var configs = new List<ConfigItem>
            {
                ConfigItem.Create("detectortype ", a.ItemArray[1], "Unique string value currently made up of one of the following: api, log, sql, email. This value specifies type of data which FIDO will parse."),
                ConfigItem.Create("detector ", a.ItemArray[2], "Detector is a more specific value such as mps, cyport, antivirus that defines the type of detector being used. Many detectors have more than one way to parse them. For instance, Cyphort has both an API and syslog. This value goes hand-in-hand with the detectortype as it tells what format (type) and then which detector (cyphort).", s => ((TextBlock)Detectors.SelectedItem).Text = $"({a.ItemArray[0]}) {s}" ),
                ConfigItem.Create("vendor ", a.ItemArray[3], "Vendor is a string value which is used as an identifier in the code, but does not server any programmatic value. It is simply a label to provide a name in certain visual functions."),
                ConfigItem.Create("server ", a.ItemArray[4], "Only required for API, log, and SQL detector types. This will point to the FQDN, URL or UNC location of the resource to ingest information from."),
                ConfigItem.Create("folder ", a.ItemArray[5], "Folder is only used with email and specifies the location of where alerts are stored after being received."),
                //ConfigItem.Create("foldertest ", a.ItemArray[6],""), 
                ConfigItem.Create("file ", a.ItemArray[7], "File is used with the log type and is the filename of the log to be parsed."),
                ConfigItem.Create("emailfrom ", a.ItemArray[8], "Only required for the email detector type. Comma separated list of email addresses that FIDO will query for new events."),
                ConfigItem.Create("lastevent ", a.ItemArray[9], "Placeholder, depending on detector, to store the most recent event stored. Used to prevent processing duplicate events."),
                ConfigItem.Create("userid ", a.ItemArray[10], "Encrypted value, if necessary, to store the userid for logging in to retrieve data."),
                ConfigItem.Create("pwd ", a.ItemArray[11], "Encrypted value, if necessary, to store the password for logging in to retrieve data."),
                ConfigItem.Create("acek ", a.ItemArray[12], "Optional encryption key to encrypt the userid and pwd if required."),
                ConfigItem.Create("db ", a.ItemArray[13], "For the SQL type, the database name from which to retrieve information."),
                ConfigItem.Create("connstring  ", a.ItemArray[14],"For the SQL type, the connection string used to connect to the external database."),
                ConfigItem.Create("query ", a.ItemArray[15], "For the API or SQL type, the query to run to retrieve data."),
                ConfigItem.Create("apikey ", a.ItemArray[18], "For the API type, the API key used to login to retrieve data.")
            };

            var sql = @"update configs_detectors set {0} = '{1}' where primkey = " + key+@";";
            ConfigBuilder.Create(ConfigGrid, configs, sql);
        }

        private void AddDetector(object sender, RoutedEventArgs e)
        {
            var sql = @"INSERT INTO configs_detectors DEFAULT VALUES;";
            DatabaseManager.Execute(sql);
            CreateMenu(Detectors.Items.Count);
        }

        private void RemoveDetector(object sender, RoutedEventArgs e)
        {
            if (Detectors.Items.Count == 0)
                return;

            if (_removeCheck)
            {
                var key = Detectors.Text.Split('(', ')')[1];
                var sql = $"DELETE FROM configs_detectors WHERE (primkey={key})";
                DatabaseManager.Execute(sql);
                CreateMenu(0);
            }
            else
            {
                ChangeRemoveButton(true);

            }
        }

        private void ChangeRemoveButton(bool check)
        {
            _removeCheck = check;

            if (check)
            {
                removeButton.Background = new SolidColorBrush(Color.FromRgb(255, 0, 0));
            }
            else
            {
                removeButton.Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
            }
        }
    }
}
