﻿using System.Windows;
using System.Windows.Controls;

namespace Fido_UI_Extension.Pages.Settings
{
    /// <summary>
    /// Interaction logic for InfoToolTip.xaml
    /// </summary>
    public partial class InfoToolTip : UserControl
    {
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string),
            typeof(InfoToolTip), new PropertyMetadata(""));

        public static readonly DependencyProperty BodyProperty = DependencyProperty.Register("Body", typeof(string),
    typeof(InfoToolTip), new PropertyMetadata(""));

        public InfoToolTip(string title, string body)
        {
            Title = title;
            Body = body;

            InitializeComponent();
            LayoutRoot.DataContext = this;
        }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public string Body
        {
            get { return (string)GetValue(BodyProperty); }
            set { SetValue(BodyProperty, value); }
        }
    }
}
