﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Fido_UI_Extension.Controls;
using Fido_UI_Extension.Data;

namespace Fido_UI_Extension.Pages.Settings
{
    /// <summary>
    /// Interaction logic for Appearance.xaml
    /// </summary>
    public partial class FidoConfig : UserControl
    {
        public FidoConfig()
        {
            InitializeComponent();

            var configValues = DatabaseManager.ReadFromTable(@"SELECT key, value from config")
                    .AsEnumerable()
                    .ToDictionary(row => row.Field<string>(0), row => row.Field<string>(1));

            var configs = new List<ConfigItem>
            {
                ConfigItem.Create("teststartup", configValues["fido.application.teststartup"], "Determines whether FIDO should run in test mode.  When running in test mode FIDO operates as normal except does not send or sends modified alerts. This is on by default.", typeof(bool)),
                ConfigItem.Create("detectors", configValues["fido.application.detectors"], "List of detectors that FIDO expects to use. If using multiple detectors they should be comma separated. e.g. detector1, detector2"),
                ConfigItem.Create("sleepiteration", configValues["fido.application.sleepiteration"], "Value in milliseconds for how long FIDO will pause between iterations, should not be negative.", typeof(int)),
            };
            
            var sql = "update config set value = '{1}' where key = 'fido.application.{0}';";

            ConfigBuilder.Create(ConfigGrid, configs, sql);
        }

        
    }
}
