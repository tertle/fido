﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Controls;
using Fido_UI_Extension.Controls;
using Fido_UI_Extension.Data;

namespace Fido_UI_Extension.Pages.Settings
{
    /// <summary>
    /// Interaction logic for Appearance.xaml
    /// </summary>
    public partial class FidoAsset : UserControl
    {
        public FidoAsset()
        {
            InitializeComponent();

            var configValues = DatabaseManager.ReadFromTable(@"SELECT key, value from config")
                    .AsEnumerable()
                    .ToDictionary(row => row.Field<string>(0), row => row.Field<string>(1));

            var configs = new List<ConfigItem>
            {
                ConfigItem.Create("hostname", configValues["fido.posture.asset.hostname"], "Comma separated string value which contains a whole, or piece, of a naming convention used to identify critical assets. For example, 'windowsdc' would be a value to input if the standard naming convention for a Windows domain controller was windowsdc101."),
                ConfigItem.Create("subnet", configValues["fido.posture.asset.subnet"], "Comma separated string value which contains a whole, or piece, of an IPv4 octet to a subnet to help identify critical assets. For example, if critical PCI subnets were on 10.10.10.x, 10.10.11.x, 10.10.12.x, then you could input in 10.10,10.11,10.12 into this field to specify them as being critical and therefore increment the score appropriately.")
            };
            
            var sql = "update config set value = '{1}' where key = 'fido.posture.asset.{0}';";

            ConfigBuilder.Create(ConfigGrid, configs, sql);
        }

        
    }
}
