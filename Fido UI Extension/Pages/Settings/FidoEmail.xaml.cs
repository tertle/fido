﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Controls;
using Fido_UI_Extension.Controls;
using Fido_UI_Extension.Data;

namespace Fido_UI_Extension.Pages.Settings
{
    /// <summary>
    /// Interaction logic for Appearance.xaml
    /// </summary>
    public partial class FidoEmail : UserControl
    {
        public FidoEmail()
        {
            InitializeComponent();

            var configValues = DatabaseManager.ReadFromTable(@"SELECT key, value from config")
                    .AsEnumerable()
                    .ToDictionary(row => row.Field<string>(0), row => row.Field<string>(1));

            var configs = new List<ConfigItem>
            {
                ConfigItem.Create("vendor", configValues["fido.email.vendor"], "Value for which email backend FIDO will connect. Currently supported values are imap and outlook."),
                ConfigItem.Create("imapserver", configValues["fido.email.imapserver"], "A fully qualified domain name (FQDN) of the mail server to connect if imap is selected in the email.vendor field."),
                ConfigItem.Create("imapport", configValues["fido.email.imapport"], "Port number to used for connecting to mail server.", typeof(int)),
                ConfigItem.Create("smtpsvr", configValues["fido.email.smtpsvr"], "SMTP server to which FIDO should send email."),
                ConfigItem.Create("smtpuserid", configValues["fido.email.smtpuserid"], "Username required to access the SMTP server"),
                ConfigItem.Create("smtppwd", configValues["fido.email.smtppwd"], "Password required to access the SMTP server"),
                ConfigItem.Create("fidoemail", configValues["fido.email.fidoemail"], "The email address assigned to FIDO for login to email to access threat events."),
                ConfigItem.Create("fidopwd", configValues["fido.email.fidopwd"], "Password for the FIDO email account"),
                ConfigItem.Create("fidoacek", configValues["fido.email.fidoacek"], "Optional encryption key to encrypt the fidopwd if required."),
                ConfigItem.Create("primaryemail", configValues["fido.email.primaryemail"],"Email values, comma separated, to which FIDO will send email alerts."),
                ConfigItem.Create("secondaryemail", configValues["fido.email.secondaryemail"], "Email values, comma separated, to which FIDO will send CC email alerts."),
                ConfigItem.Create("nonalertemail", configValues["fido.email.nonalertemail"], "Email values, comma separated, to which FIDO will send updates about non-alert information."),
                ConfigItem.Create("helpdeskemail", configValues["fido.email.helpdeskemail"], "If auto-ticket generation is enabled by a ticketing solution, then the email address used to send a FIDO alert for ticket creation."),
                ConfigItem.Create("erroremail", configValues["fido.email.erroremail"], "When FIDO has failures, the email address to send issues."),
                ConfigItem.Create("runerroremail", configValues["fido.email.runerroremail"], "Boolean value for whether to send error emails.", typeof(bool))
            };
            

            var sql = "update config set value = '{1}' where key = 'fido.email.{0}';";
            ConfigBuilder.Create(ConfigGrid, configs, sql);
        }

        
    }
}
