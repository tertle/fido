﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Controls;
using Fido_UI_Extension.Controls;
using Fido_UI_Extension.Data;

namespace Fido_UI_Extension.Pages.Settings
{
    /// <summary>
    /// Interaction logic for Appearance.xaml
    /// </summary>
    public partial class FidoMachine : UserControl
    {
        public FidoMachine()
        {
            InitializeComponent();

            var configValues = DatabaseManager.ReadFromTable(@"SELECT key, value from config")
                    .AsEnumerable()
                    .ToDictionary(row => row.Field<string>(0), row => row.Field<string>(1));

            var configs = new List<ConfigItem>
            {
                ConfigItem.Create("criticalpatches", configValues["fido.posture.machine.criticalpatches"], "If the total number of critical patches needed is equal to, or greater than, this number, then it will trigger adding value to the machine posture score.", typeof(int)),
                ConfigItem.Create("criticalpatchesweight", configValues["fido.posture.machine.criticalpatchesweight"], "Integer value which will be added to the machine posture score if the total number of critical patches is equaled or exceeded.", typeof(int)),
                ConfigItem.Create("highpatches", configValues["fido.posture.machine.highpatches"], "If the total number of high patches needed is equal to, or greater than, this number, then it will trigger adding value to the machine posture score.", typeof(int)),
                ConfigItem.Create("highpatchesweight", configValues["fido.posture.machine.highpatchesweight"], "Integer value which will be added to the machine posture score if the total number of high patches is equaled or exceeded.", typeof(int)),
                ConfigItem.Create("lowpatches", configValues["fido.posture.machine.lowpatches"], "If the total number of low patches needed is equal to, or greater than, this number, then it will trigger adding value to the machine posture score.", typeof(int)),
                ConfigItem.Create("lowpatchesweight", configValues["fido.posture.machine.lowpatchesweight"], "Integer value which will be added to the machine posture score if the total number of low patches is equaled or exceeded.", typeof(int)),
                ConfigItem.Create("avnotinstalled", configValues["fido.posture.machine.avnotinstalled"], "Integer value added to the machine score if antivirus is determined to not be installed.", typeof(int)),
                ConfigItem.Create("avnotruning", configValues["fido.posture.machine.avnotruning"], "Integer value added to the machine score if antivirus is determined to not be running.", typeof(int))
            };
            
            var sql = "update config set value = '{1}' where key = 'fido.posture.machine.{0}';";

            ConfigBuilder.Create(ConfigGrid, configs, sql);
        }

        
    }
}
