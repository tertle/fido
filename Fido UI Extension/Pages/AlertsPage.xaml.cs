﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;

namespace Fido_UI_Extension.Pages
{
    /// <summary>
    /// Interaction logic for AlertsPage.xaml
    /// </summary>
    public partial class AlertsPage : UserControl
    {
        public static string Page = string.Empty;

        public AlertsPage()
        {
            InitializeComponent();

            var sql = "SELECT event_alerts.primkey, event_threat.threat_name, event_alerts.timestamp FROM event_alerts LEFT OUTER JOIN event_threat ON event_alerts.primkey = event_threat.primkey;";
            var list = DatabaseManager.ReadFromTable(sql).AsEnumerable().Reverse();

            foreach (DataRow s in list)
            {
                var dt = Convert.ToDateTime(s.ItemArray[2]);

                var link = new Link() { DisplayName = $"{dt.ToString("yyyy-MM-dd")} {s.ItemArray[1]}", Source = new Uri($"/Pages/AlertPage.xaml?x={s.ItemArray[0]}", UriKind.Relative) };
                List.Links.Add(link);
            }

            List.SelectedSourceChanged += ListOnSelectedSourceChanged;
        }

        private void ListOnSelectedSourceChanged(object sender, SourceEventArgs sourceEventArgs)
        {
            var url = sourceEventArgs.Source.OriginalString;
            var queryString = url.Substring(url.IndexOf('?')).Split('#')[0];
            var queryDictionary = System.Web.HttpUtility.ParseQueryString(queryString);
            Page = queryDictionary["x"];
        }
    }
}
