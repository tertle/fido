﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Controls;

namespace Fido_UI_Extension.Pages.Alerts
{
    /// <summary>
    /// Interaction logic for AlertPage.xaml
    /// </summary>
    public partial class ThreatInformationPage : UserControl
    {
        private readonly AutoList<object> _threatValues;

        public ThreatInformationPage()
        {
            InitializeComponent();

            DataContext = this;

            var sql = $"SELECT * FROM event_threat WHERE primkey = {AlertsPage.Page}";
            _threatValues = new AutoList<object>(DatabaseManager.ReadFromTable(sql).AsEnumerable().FirstOrDefault()?.ItemArray);
        }

        public object ThreatISP => _threatValues[1];
        public object ThreatName => _threatValues[2];

        public int ThreatScore
        {
            get
            {
                int i;
                int.TryParse(_threatValues[3].ToString(), out i);
                return i;
            }
        }

        public object Detector => _threatValues[4];
        public object ThreatURL => _threatValues[5];
        public object ThreatHash => _threatValues[6];
        public object Time => _threatValues[7];
        public object ActionTaken => _threatValues[8];
        public object FileName => _threatValues[9];
        public object ThreatStatus => _threatValues[10];
    }
}
