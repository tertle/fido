﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Controls;

namespace Fido_UI_Extension.Pages.Alerts
{
    /// <summary>
    /// Interaction logic for AlertPage.xaml
    /// </summary>
    public partial class UserInformationPage : UserControl
    {
        private readonly AutoList<object> _threatValues;

        public UserInformationPage()
        {
            InitializeComponent();

            DataContext = this;

            var sql = $"SELECT * FROM event_user WHERE primkey = {AlertsPage.Page}";
            _threatValues =
                new AutoList<object>(DatabaseManager.ReadFromTable(sql).AsEnumerable().FirstOrDefault()?.ItemArray);
        }

        public object Username => _threatValues[1];
        public object FullName => _threatValues[2];
        public object Email => _threatValues[3];
        public object Title => _threatValues[4];
        public object Department => _threatValues[5];
        public object EmpType => _threatValues[6];
        public object EmpPhone => _threatValues[7];
        public object Cube => _threatValues[8];
        public object City => _threatValues[9];
        public object Manager => _threatValues[10];
        public object ManagerTitle => _threatValues[11];
        public object ManagerEmail => _threatValues[12];
        public object ManagerPhone => _threatValues[13];

        public int UserScore
        {
            get
            {
                int i;
                int.TryParse(_threatValues[14].ToString(), out i);
                return i;
            }

        }
    }
}
