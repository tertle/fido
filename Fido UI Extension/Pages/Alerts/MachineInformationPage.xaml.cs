﻿using System.Data;
using System.Linq;
using System.Windows.Controls;

namespace Fido_UI_Extension.Pages.Alerts
{
    /// <summary>
    /// Interaction logic for AlertPage.xaml
    /// </summary>
    public partial class MachineInformationPage : UserControl
    {
        private readonly AutoList<object> _machineValues;

        public MachineInformationPage()
        {
            InitializeComponent();

            DataContext = this;

            var sql = $"SELECT * FROM event_machine WHERE event_machine.primkey = {AlertsPage.Page}";
            _machineValues = new AutoList<object>(DatabaseManager.ReadFromTable(sql).AsEnumerable().FirstOrDefault()?.ItemArray);
        }

        public object MachineName => _machineValues[1];
        public object OS => _machineValues[2];
        public object Domain => _machineValues[3];

        public object Patches
        {
            get
            {
                var s = string.Empty;
                if (_machineValues[4] != null) s += _machineValues[4];
                if (_machineValues[5] != null) s += (string.IsNullOrEmpty(s) ? "" : ", ") + _machineValues[5] ;
                if (_machineValues[6] != null) s += (string.IsNullOrEmpty(s) ? "" : ", ") + _machineValues[6];
                return s;
            }
        }

        public object AVInstalled => _machineValues[7];
        public object AVRunning => _machineValues[8];
        public object AVVersion => _machineValues[9];
        public object Bit9Installed => _machineValues[10];

        public int MachineScore
        {
            get
            {
                int i;
                int.TryParse(_machineValues[11].ToString(), out i);
                return i;
            }
        }
    }
}
