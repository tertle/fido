﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Controls;

namespace Fido_UI_Extension.Pages.Alerts
{
    /// <summary>
    /// Interaction logic for AlertPage.xaml
    /// </summary>
    public partial class AlertInformationPage : UserControl
    {
        private readonly AutoList<object> _alertValues;

        public AlertInformationPage()
        {
            InitializeComponent();

            DataContext = this;

            var sql = $"SELECT * FROM event_alerts WHERE primkey = {AlertsPage.Page}";
            _alertValues = new AutoList<object>(DatabaseManager.ReadFromTable(sql).AsEnumerable().FirstOrDefault()?.ItemArray);
        }

        public object SourceIP => _alertValues[2];
        public object HostName => _alertValues[3];

        public int TotalScore
        {
            get
            {
                int i;
                int.TryParse(_alertValues[5].ToString(), out i);
                return i;
            }
        }

        public object Time => _alertValues[4];
        public object AlertID => _alertValues[6];
    }
}
