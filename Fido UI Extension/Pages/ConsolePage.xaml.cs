﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using TextBox = System.Windows.Controls.TextBox;
using UserControl = System.Windows.Controls.UserControl;

namespace Fido_UI_Extension.Pages
{
    /// <summary>
    /// Interaction logic for Console.xaml
    /// </summary>
    public partial class ConsolePage : UserControl
    {
        public ConsolePage()
        {
            InitializeComponent();

            SetupConsole();
        }

        private void SetupConsole()
        {
            ConsoleListBox.Items.Clear();

            foreach (var l in ConsoleManager.ConsoleOut.GetLines())
            {
                //ConsoleListBox.Items.Add(new TextBox { Text = l, IsReadOnly = true, BorderThickness = new Thickness(0)});
                ConsoleListBox.Items.Add(new ListBoxItem { Content = l, });
            }

            ConsoleManager.ConsoleOut.OnWriteLine += ConsoleOutOnOnWriteLine;
        }

        private void ConsoleOutOnOnWriteLine(string s)
        {
            try
            {
                if (Dispatcher.HasShutdownStarted)
                    return;
                Dispatcher?.Invoke(() => ConsoleListBox?.Items.Add(new TextBox {Text = s, IsReadOnly = true}));
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
