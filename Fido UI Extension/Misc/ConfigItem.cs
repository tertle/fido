﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fido_UI_Extension.Data
{
    public class ConfigItem
    {
        public string Key { get; }
        public string Value { get; }
        public string Tooltip { get; }
        public Action<string> OnChange { get; }
        public Type Type { get; }

        private ConfigItem(string key, string value, string tooltip, Type type, Action<string> onChange = null)
        {
            Key = key;
            Value = value;
            Tooltip = tooltip;
            Type = type;
            OnChange = onChange;
        }

        public static ConfigItem Create(string key, object value, string tooltip, Action<string> onChange = null)
        {
            var s = string.Empty;

            if (value != null)
                s = value.ToString();

            return new ConfigItem(key, s, tooltip, typeof(string), onChange);
        }

        public static ConfigItem Create(string key, object value, string tooltip, Type type, Action<string> onChange = null)
        {
            var s = string.Empty;

            if (value != null)
                s = value.ToString();

            return new ConfigItem(key, s, tooltip, type, onChange);
        }
    }


}
