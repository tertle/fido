﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Fido_UI_Extension
{
    public class ChartViewModel : INotifyPropertyChanged
    {
        private object _numberOfEvents;
        private object _eventsPerDay;
        private object _averageScore;
        private object _averageThreat;
        private object _averageUser;
        private object _averageMachine;
        private object _worstScore;
        private object _mostTargettedOs;
        private object _mostTargettedMachine;
        private object _worstMachine;
        private object _worstUser;
        private object _worstThreat;

        public ChartViewModel()
        {
            GetTimes(TimeSpan.FromDays(7));
            GetTables();
        }

        private void GetTimes(TimeSpan timeSpan)
        {
            var time = DateTime.Now.ToUniversalTime() - timeSpan;

            var allEvents =
                DatabaseManager.ReadFromTable(@"SELECT primkey, timestamp, previous_score FROM event_alerts")
                    .AsEnumerable();

            EnumerableRowCollection<DataRow> eventsWithinTimespan =
                allEvents.Where(row => Convert.ToDateTime(row.Field<string>(1)) > time);

            NumberOfEvents = eventsWithinTimespan.Count();
            EventsPerDay = Math.Round(eventsWithinTimespan.Count()/(decimal) timeSpan.Days, 2);

            var ids = string.Join(",", eventsWithinTimespan.Select(row => row.Field<long>(0)));

            var scores = eventsWithinTimespan.Select(row => row.Field<string>(2))
                .ToList()
                .ConvertAll(Convert.ToInt32);

            AverageScore = Math.Round(scores.DefaultIfEmpty().Average());
            WorstScore = scores.DefaultIfEmpty().Max();

            var threats = DatabaseManager.ReadFromTable(
                $"SELECT threat_score FROM event_threat WHERE primkey IN ({ids})")
                .AsEnumerable()
                .Select(row => row.Field<string>(0))
                .ToList()
                .ConvertAll(Convert.ToInt32);

            AverageThreat = Math.Round(threats.DefaultIfEmpty().Average());
            WorstThreat = threats.DefaultIfEmpty().Max();

            var user = DatabaseManager.ReadFromTable($"SELECT user_score FROM event_user WHERE primkey IN ({ids})")
                .AsEnumerable()
                .Select(row => row.Field<string>(0))
                .ToList()
                .ConvertAll(Convert.ToInt32);

            AverageUser = Math.Round(user.DefaultIfEmpty().Average());
            WorstUser = user.DefaultIfEmpty().Max();
            
            var machineData = DatabaseManager.ReadFromTable(
                $"SELECT machine_score, hostname, os FROM event_machine WHERE primkey IN ({ids})")
                .AsEnumerable();

            var machine = machineData.Select(row => row.Field<string>(0))
                .ToList()
                .ConvertAll(Convert.ToInt32);

            AverageMachine = Math.Round(machine.DefaultIfEmpty().Average());
            WorstMachine = machine.DefaultIfEmpty().Max();

            MostTargettedMachine = machineData.Select(row => row.Field<string>(1))
                .GroupBy(x => x)
                .OrderByDescending(x => x.Count())
                .First().Key;

            MostTargettedOS = machineData.Select(row => row.Field<string>(2))
                .GroupBy(x => x)
                .OrderByDescending(x => x.Count())
                .First().Key;

        }

        private void GetTables()
        {
            var allEvents =
                DatabaseManager.ReadFromTable(@"SELECT primkey, timestamp, previous_score FROM event_alerts")
                    .AsEnumerable();

            EnumerableRowCollection<DateTime> eventsInLastYear =
                allEvents.Select(row => Convert.ToDateTime(row.Field<string>(1)));

            var month = DateTime.Today;

            EventsPerMonth.Clear();

            for (var i = 11; i >= 0; i--)
            {
                var m = month.AddMonths(-i);

                var c = eventsInLastYear.Count(d => d.Month == m.Month && d.Year == m.Year);
                EventsPerMonth.Add(
                    new KeyValuePair<string, int>(m.ToString("MMM", CultureInfo.InvariantCulture).Substring(0, 3), c));
            }
            
            var idsInLastYear = allEvents
                .Where(
                    row =>
                        Convert.ToDateTime(row.Field<string>(1)) >
                        DateTime.Now.ToUniversalTime() - TimeSpan.FromDays(365))
                .Select(row => row.Field<long>(0));

            var idsLastYear = string.Join(",", idsInLastYear);

            var machineDataPastYear = DatabaseManager.ReadFromTable(
                $"SELECT hostname FROM event_machine WHERE primkey IN ({idsLastYear})")
                .AsEnumerable();

            var lastYearMachineAttacks = machineDataPastYear.Select(row => row.Field<string>(0))
                .GroupBy(x => x)
                .OrderByDescending(x => x.Count())
                .ToList();

            // Show 5 machines max
            var max = Math.Min(lastYearMachineAttacks.Count, 5);

            EventsPerMachine.Clear();

            for (var i = 0; i < max; i++)
            {
                var l = lastYearMachineAttacks[i];
                var n = l.Key;

                Legend.Add(n);

                var c = l.Count();
                EventsPerMachine.Add(new KeyValuePair<string, int>(n, c));
            }
        }

        public object NumberOfEvents
        {
            get { return _numberOfEvents; }
            private set
            {
                _numberOfEvents = value;
                OnPropertyChanged();
            }
        }

        public object EventsPerDay
        {
            get { return _eventsPerDay; }
            private set
            {
                _eventsPerDay = value;
                OnPropertyChanged();
            }
        }

        public object AverageScore
        {
            get { return _averageScore; }
            private set
            {
                _averageScore = value;
                OnPropertyChanged();
            }
        }

        public object AverageThreat
        {
            get { return _averageThreat; }
            private set
            {
                _averageThreat = value;
                OnPropertyChanged();
            }
        }

        public object AverageUser
        {
            get { return _averageUser; }
            private set
            {
                _averageUser = value;
                OnPropertyChanged();
            }
        }

        public object AverageMachine
        {
            get { return _averageMachine; }
            private set
            {
                _averageMachine = value;
                OnPropertyChanged();
            }
        }

        public object WorstScore
        {
            get { return _worstScore; }
            private set
            {
                _worstScore = value;
                OnPropertyChanged();
            }
        }

        public object WorstThreat
        {
            get { return _worstThreat; }
            private set
            {
                _worstThreat = value;
                OnPropertyChanged();
            }
        }

        public object WorstUser
        {
            get { return _worstUser; }
            private set
            {
                _worstUser = value;
                OnPropertyChanged();
            }
        }

        public object WorstMachine
        {
            get { return _worstMachine; }
            private set
            {
                _worstMachine = value;
                OnPropertyChanged();
            }
        }

        public object MostTargettedMachine
        {
            get { return _mostTargettedMachine; }
            private set
            {
                _mostTargettedMachine = value;
                OnPropertyChanged();
            }
        }

        public object MostTargettedOS
        {
            get { return _mostTargettedOs; }
            private set
            {
                _mostTargettedOs = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<KeyValuePair<string, int>> EventsPerMonth { get; private set; } =
            new ObservableCollection<KeyValuePair<string, int>>();

        public ObservableCollection<KeyValuePair<string, int>> EventsPerMachine { get; private set; } =
            new ObservableCollection<KeyValuePair<string, int>>();

        public ObservableCollection<string> Legend { get; private set; } = new ObservableCollection<string>();

        public void PeriodChanged(string t)
        {
            switch (t)
            {
                case "Week":
                    GetTimes(TimeSpan.FromDays(7));
                    break;
                case "Month":
                    GetTimes(TimeSpan.FromDays(30));
                    break;
                case "Year":
                    GetTimes(TimeSpan.FromDays(365));
                    break;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}