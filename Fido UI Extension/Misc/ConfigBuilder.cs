﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Fido_UI_Extension.Data;
using Fido_UI_Extension.Pages.Settings;

namespace Fido_UI_Extension.Controls
{
    public static class ConfigBuilder
    {
        public static void Create(Grid grid, List<ConfigItem> configs, string sqlUpdate)
        {
            grid.Children.Clear();

            for (int rowIndex = 0; rowIndex < configs.Count; rowIndex++)
            {
                var config = configs[rowIndex];

                //add a new row to the grid
                var newRow = new RowDefinition {Height = new GridLength(0, GridUnitType.Auto)};
                grid.RowDefinitions.Add(newRow);

                var key = new TextBlock()
                {
                    Text = config.Key,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(0, 0, 16, 4),
                    ToolTip = new InfoToolTip($"{config.Key} ({config.Type.Name.ToLower()})", config.Tooltip)
                };

                var valueBefore = config.Value;

                var tbProductName = new TextBox
                {
                    Text = valueBefore,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(0, 0, 16, 4), MinWidth = 200
                };

                tbProductName.LostFocus += (sender, args) =>
                {
                    if (valueBefore == tbProductName.Text)
                        return;

                    if (config.Type == typeof (int))
                    {
                        int i;
                        if (!int.TryParse(tbProductName.Text, out i))
                        {
                            tbProductName.Text = valueBefore;
                            return;
                        }
                    }
                    else if (config.Type == typeof (bool))
                    {
                        bool b;
                        if (!bool.TryParse(tbProductName.Text, out b))
                        {
                            tbProductName.Text = valueBefore;
                            return;
                        }
                    }

                    var sql = string.Format(sqlUpdate, config.Key, tbProductName.Text);
                    DatabaseManager.Execute(sql);

                    config.OnChange?.Invoke(tbProductName.Text);
                };

                //set the row and column positions for all the 3 controls
                Grid.SetRow(key, rowIndex);
                Grid.SetColumn(key, 0);

                Grid.SetRow(tbProductName, rowIndex);
                Grid.SetColumn(tbProductName, 1);

                //add the controls to the grid controls colelction
                grid.Children.Add(key);
                grid.Children.Add(tbProductName);
            }
        }
    }
}
