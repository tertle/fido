﻿using System.Data;
using System.Data.SQLite;
using System.Linq;

namespace Fido_UI_Extension
{
    static class DatabaseManager
    {
        private static readonly string ConnectionString;

        static DatabaseManager()
        {
            string database = System.AppDomain.CurrentDomain.BaseDirectory + @"data\fido.db";
            ConnectionString = $"Data Source={database};Version=3;";
        }

        public static DataTable ReadFromTable(string sql)
        {
            var dt = new DataTable();
            var conn = new SQLiteConnection(ConnectionString);
            conn.Open();
            var mycommand = new SQLiteCommand(conn) { CommandText = sql };
            var reader = mycommand.ExecuteReader();
            dt.Load(reader);

            reader.Close();
            conn.Close();

            return dt;
        }

        public static void Execute(string sql)
        {
            var conn = new SQLiteConnection(ConnectionString);
            conn.Open();
            var mycommand = new SQLiteCommand(conn) { CommandText = sql };
            mycommand.ExecuteNonQuery();
            conn.Close();
        }
    }
}
