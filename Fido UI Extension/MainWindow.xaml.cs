﻿using System.Data.SQLite;
using System.IO;
using System.Threading;
using System.Windows;
using Fido_Main;
using FirstFloor.ModernUI.Windows.Controls;

namespace Fido_UI_Extension
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ModernWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            Setup();
            StartFido();
        }

        private void Setup()
        {
            // Check the database exists
            var directory = System.AppDomain.CurrentDomain.BaseDirectory + @"data\";
            var database = directory + @"fido.db";
            if (!File.Exists(database))
            {
                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                SQLiteConnection.CreateFile(database);

                SQLiteConnection mDbConnection = new SQLiteConnection($"Data Source={database};Version=3;");
                mDbConnection.Open();

                const string createsql = "CREATE TABLE config(key TEXT NOT NULL, value TEXT, PRIMARY KEY(key))";
                var command = new SQLiteCommand(createsql, mDbConnection);
                command.ExecuteNonQuery();

                mDbConnection.Close();
            }
        }

        private void StartFido()
        {
            ConsoleManager.Setup(); // The Fido app requires a console, so we setup a hidden one         

            bool execute = true;
            Dispatcher.ShutdownStarted += (sender, args) => execute = false;
            new Thread(() => Fido.Execute(() => execute)).Start();
        }
    }
}
