﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Fido_UI_Extension
{
    public class AutoList<TU>
    {
        public delegate TU DefaultValue();

        private readonly DefaultValue _defaultValue;
        private readonly List<TU> _list = new List<TU>();

        public AutoList()
        {
            _defaultValue = () => default(TU);
        }

        public AutoList(IEnumerable<TU> list)
        {
            _list = list == null ? new List<TU>() : new List<TU>(list);

            _defaultValue = () => default(TU);
        }

        public TU this[int key] => key < 0 || key >= _list.Count ? _defaultValue() : _list[key];
    }
}